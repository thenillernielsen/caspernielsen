from digi.xbee.models.status import NetworkDiscoveryStatus
from digi.xbee.devices import XBeeDevice
from time import sleep

module = XBeeDevice('com6', 9600)

try:
    module.open()
    xbee_network = module.get_network()
    xbee_network.set_discovery_timeout(10)
    xbee_network.clear()

    def device_discovered(remote):
            print("Module found on network:", remote)

    def discovery_done(discovery_status):
        if discovery_status == NetworkDiscoveryStatus.SUCCESS:
            print("Network discovery finished")
        else:
            pass

    xbee_network.add_device_discovered_callback(device_discovered)

    xbee_network.add_discovery_process_finished_callback(discovery_done)

    xbee_network.start_discovery_process()

    print("Looking for module on network")
    while xbee_network.is_discovery_running():
        sleep(0.1)
finally:
    if module is not None and module.is_open():
        module.close()
