# This code is used on the coordinator Xbee module, this program is responsible for sending the turn on/off instructions
# Note that for both the programs to work together,
# they should both be connected via two usb adapters to a windows machine

from digi.xbee.devices import XBeeDevice
from time import sleep


def main():
    # Creating variables for the devices
    device = XBeeDevice('COM5', 9600)
    ep_device_id = "CONTROL"
    # Opening the connection so xbee can receive instructions
    device.open()
    # Obtain the remote XBee device from the XBee network.
    xbee_network = device.get_network()
    ep_device = xbee_network.discover_device(ep_device_id)
    if ep_device is None:
        print("Could not find the remote device")
        exit(1)

    def data_received(xbee_message):
        # Optains information on the device that sends response
        response_device = str((xbee_message.ep_device.get_64bit_addr()))
        # Optains the contents of the message
        data = str((xbee_message.data.decode()))
        print('\n', 'Response from', response_device, ':', data)

    device.add_data_received_callback(data_received)
    try:
        while True:
            input_to_send = str(input("message to send:"))
            # Checks length of message, if correct length it sends the data to the endpoint
            if len(input_to_send) == 1:
                print("Sending data to %s : %s..." % (ep_device.get_64bit_addr(), input_to_send))
                device.send_data(ep_device, input_to_send)
                sleep(1)
            else:
                print('can only send one char at a time')

    except KeyboardInterrupt():
        device.close()


if __name__ == '__main__':
    main()
