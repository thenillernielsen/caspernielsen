from digi.xbee.devices import XBeeDevice
from time import sleep

device = XBeeDevice('COM5', 9600)
ep_device_id = "CONTROL"

device.open()
xbee_network = device.get_network()
ep_device = xbee_network.discover_device(ep_device_id)
if ep_device is None:
    print("Could not find the remote device")
    exit(1)

while True:
    input_to_send = str(input("message to send:"))     
    print(f'Sending data to{ep_device.get_64bit_addr()}: {input_to_send}')
    device.send_data(ep_device, input_to_send)
    sleep(1)
