from digi.xbee.devices import XBeeDevice
from digi.xbee.io import IOLine, IOMode

module = XBeeDevice("COM5", 9600)
control_device_id = "CONTROL"

module.open()
xbee_network = module.get_network()
control_device = xbee_network.discover_device(control_device_id)

if control_device is None:
    print("Could not find the remote module")
    exit(1)

print('waiting for data')

def data_received(xbee_message):
    data = str(xbee_message.data.decode())
    print('message received:', data)

module.add_data_received_callback(data_received)

input()

