# This program is used to discover devices on the zigbee network,
# it will print out the mac addresses of the found devices.
# This code can in principal be run on any of the devices, 
# but in this case it is set to run from the Coordinator device
from digi.xbee.models.status import NetworkDiscoveryStatus
from digi.xbee.devices import XBeeDevice
from time import sleep


def main():
    # defining port and the baud rate
    cord_xbee = XBeeDevice('com4', 9600)

    try:
        # Tries to establish serial connection
        cord_xbee.open()
        # Gets network via coordinator
        xbee_network = cord_xbee.get_network()
        # Timeout in seconds
        xbee_network.set_discovery_timeout(10)
        # Clearing the network so old data wont cause problems
        xbee_network.clear()

        # if devices is discovered
        def device_discovered(remote):
            print("Device found on network:", str(remote))

        # When the discovery is finished
        def discovery_done(discovery_status):
            if discovery_status == NetworkDiscoveryStatus.SUCCESS:
                print("Network discovery finished")
            else:
                print(str(discovery_status.description))

        xbee_network.add_device_discovered_callback(device_discovered)

        xbee_network.add_discovery_process_finished_callback(discovery_done)

        xbee_network.start_discovery_process()

        print("Looking for devices on network")
        # To slow down execute process allowing time to discover network
        while xbee_network.is_discovery_running():
            sleep(0.1)
    # Stop threads and close serial ports
    finally:
        if cord_xbee is not None and cord_xbee.is_open():
            cord_xbee.close()


if __name__ == '__main__':
    main()
