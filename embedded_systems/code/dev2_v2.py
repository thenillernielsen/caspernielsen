# This code is used on the Xbee module that is connected to the breadboard with the LED circuit.
# Note that for both the programs to work together,
# they should both be connected via two usb adapters to a windows machine

from digi.xbee.devices import XBeeDevice
from digi.xbee.io import IOLine, IOMode


def main():
    # Creating variables for the devices and LED
    device = XBeeDevice("COM5", 9600)
    control_device_id = "CONTROL"
    led = IOLine.DIO4_AD4
    # Opening the connection so xbee can receive instructions
    device.open()
    # Sets the GPIO of the LED pin to low
    device.set_io_configuration(led, IOMode.DIGITAL_OUT_LOW)
    # Scanning network from device
    xbee_network = device.get_network()
    # Discovers the coordinator xbee by using the name, and stores that in variable
    control_device = xbee_network.discover_device(control_device_id)
    if control_device is None:
        print("Could not find the remote device")
        exit(1)

    print('waiting for data')

    def data_received(xbee_message):
        data = str((xbee_message.remote_device.get_64bit_addr(), xbee_message.data.decode()))
        data_index = (data[-3])
        print('message received:', data_index)
        # Checks if the data from the message contains the values to turn on/off LED
        if data_index == 'p':
            # Turns the pin output that is connected to the LED high
            device.set_io_configuration(led, IOMode.DIGITAL_OUT_HIGH)
            led_status = str("LED ON")
            # Sends LED status back to the coordinator
            print("Sending response: %s to %s..." % (led_status, control_device.get_64bit_addr()))
            device.send_data(control_device, led_status)
        elif data_index == 'o':
            # Turns the pin output that is connected to the LED low
            device.set_io_configuration(led, IOMode.DIGITAL_OUT_LOW)
            led_status = str("LED OFF")
            # Sends LED status back to the coordinator
            print("Sending response: %s to %s..." % (led_status, control_device.get_64bit_addr()))
            device.send_data(control_device, led_status)

    device.add_data_received_callback(data_received)

    input()


if __name__ == '__main__':
    main()
