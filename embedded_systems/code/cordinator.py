from digi.xbee.devices import XBeeDevice
from time import sleep

module = XBeeDevice('COM6', 9600)
ep_device_id = "REMOTE"
module.open()
xbee_network = module.get_network()
ep_device = xbee_network.discover_device(ep_device_id)

if ep_device is None:
    print("Could not find the remote module")
    exit(1)

def data_received(xbee_message):
    response_module = str((xbee_message.ep_device.get_64bit_addr()))
    data = str((xbee_message.data.decode()))
    print('\n', 'Response from', response_module, ':', data)

module.add_data_received_callback(data_received)
while True:
    input_to_send = str(input("message to send:"))
    print("Sending data to %s : %s..." % (ep_device.get_64bit_addr(), input_to_send))
    module.send_data(ep_device, input_to_send)
    sleep(1)