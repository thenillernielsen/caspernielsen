class SendClass:
    def __init__(self, serial_connection, move_command):
        self.serial_connection = serial_connection
        #self.move_command = move_command

    def send_data(self, move_command):
        msg_data = move_command
        if msg_data != None:
            byte_msg = msg_data.encode('utf-8')
            self.serial_connection.write(byte_msg)
            print("msg sent", byte_msg, msg_data)

 