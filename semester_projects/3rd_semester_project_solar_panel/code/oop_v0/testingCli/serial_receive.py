class ReceiveClass:
    def __init__(self, serial_connection):
        self.serial_connection = serial_connection

    def packet_receive(self):
        data = self.serial_connection.readline()
        self.clean_data = str(data, "utf-8")
        if self.clean_data is not None:
            print(self.clean_data)

    def splitter(self):
        """
		takes in a string, splits the data on "," and returns the values of x, y and z

		"""
        split_packet = self.clean_data.split(",")
        self.x = float(split_packet[0])
        self.y = float(split_packet[1])
        self.z = float(split_packet[2])
