import serial


def serial_setup():
	"""
	takes in none, innitializes a serial connection variable from the port and the baudrate
	with a 5 sec timeout
	returns the serial connection

	"""
	port = "COM2"
	baudrate = 115200
	timeout = 5
	serial_connection = serial.Serial(port, baudrate)
	serial_connection.open()
	return serial_connection


def packet_receive(serial_connection):
	"""
	takes in the serial connection object
	returns data gathered from the serial port
	should be used in a loop maybe also maybe .readline should be replaced with .read instead

	"""
	data = serial_connection.readline()
	if data not None:
		return data


def splitter(data):
	"""
	takes in a string, converts it to a string with utf-8 decoding 
	splits the data on "," and returns a dictionary with the individual values

	"""
	data = str(data, "utf-8")
	clean_data = data.split(",")
	return clean_data


def port_close(serial_connection)
	if serial_connection.is_open:
		serial_connection.close()



"""
while (True):
    if (serial_connection.in_waiting() > 0): # if incoming bytes are waiting to be read from the serial input buffer
        data_str = serial_connection.read(serial_connection.inWaiting()).decode('ascii') 
        print(data_str, end='') 
"""