# Using library for thingspeak from https://github.com/mchwalisz/thingspeak
import thingspeak
import time
import sys
import RPi.GPIO as GPIO
import time
import Freenove_DHT as DHT
red_led = 15  #defining GPIO 15 for red led
yellow_led = 13  #defining GPIO 13 for yellow led
green_led = 16  #defining GPIO 16 for green led
DHTPin = 11 #defining GPIO 11 for the temperature sensor
file_name = 'user_def_values.txt'  #setting up the file name
file_hand = open(file_name)
user_temp_min = 0  #setting the user input values for temperature and humidity
user_temp_max = 0  #to the default values
user_humi_min = 0
user_humi_max = 0
tsloop_duration = 15000

def now_ms():
    ms = int(time.time() * 1000)
    return ms

# open and format write key
def get_key(filename, out=sys.stdout):
    out.write('using key path {}\n'.format(filename))
    with open(filename) as f:
        key = f.readline()
    out.write("- read key with {} chars\n".format(len(key.strip())))
    return key.strip()


for line in file_hand:
    if line.startswith('min temperature'):
        single_word = line.split()
        for word in single_word:
            if word.isdigit():
                user_temp_min = word
                number_temp_min = float(user_temp_min)
                print('min temperature', number_temp_min)
    if line.startswith('max temperature'):
        single_word = line.split()
        for word in single_word:
            if word.isdigit():
                user_temp_max = word
                number_temp_max = float(user_temp_max)
                print('max temperature', number_temp_max)
    if line.startswith('min humidity'):
        single_word = line.split()
        for word in single_word:
            if word.isdigit():
                user_humi_min = word
                number_humi_min = float(user_humi_min)
                print('min humidity', number_humi_min)
    if line.startswith('max humidity'):
        single_word = line.split()
        for word in single_word:
            if word.isdigit():
                user_humi_max = word
                number_humi_max = float(user_humi_max)
                print('max humidity', number_humi_max)
            

def setup():
    GPIO.setmode(GPIO.BOARD)       # use PHYSICAL GPIO Numbering
    GPIO.setup(red_led, GPIO.OUT)   # set the ledPin to OUTPUT mode
    GPIO.output(red_led, GPIO.LOW)  # make ledPin output LOW level
    GPIO.setup(yellow_led, GPIO.OUT)   # set the ledPin to OUTPUT mode
    GPIO.output(yellow_led, GPIO.LOW)
    GPIO.setup(green_led, GPIO.OUT)   # set the ledPin to OUTPUT mode
    GPIO.output(green_led, GPIO.LOW)

channel_id = get_key('channel.txt')
write_key = get_key('write_key.txt')

def loop():
    dht = DHT.DHT(DHTPin)   #create a DHT class object
    sumCnt = 0 #number of reading times
    tsloop_start = now_ms() - 15000
    while(True):
        sumCnt += 1         #counting number of reading times
        chk = dht.readDHT11()
        #read DHT11 and get a return value. Then determine whether data read is normal according to the return value.
        print ("Data points collected : %d, \t chk    : %d"%(sumCnt,chk))
        if (chk is dht.DHTLIB_OK):      #read DHT11 and get a return value. Then determine whether data read is normal according to the return value.
            if int(time.time() * 1000) - tsloop_start > tsloop_duration:
                # establishing the connection to thingspeak
                channel = thingspeak.Channel(id=channel_id, api_key=write_key)
                response = channel.update({1: dht.temperature, 2: dht.humidity})
                print(response)
                

            else:
                continue
            #if temperature exceedes the boundaries set, the red led will turn on
            if dht.temperature<number_temp_min or dht.temperature>number_temp_max:
                GPIO.output(red_led, GPIO.HIGH)
                GPIO.output(green_led, GPIO.LOW)
                print("RED LED ON!!")

            #if humidity exceedes the boundaries set, the yellow led will turn on    
            elif (dht.humidity<number_humi_min or dht.humidity>number_humi_max):
                GPIO.output(yellow_led, GPIO.HIGH)
                GPIO.output(green_led, GPIO.LOW)
                print("YELLOW LED ON!!")
            #if both humidity and temperature respect the boundaries set by the user, green led will be on
            else:
                GPIO.output(red_led, GPIO.LOW)
                GPIO.output(yellow_led, GPIO.LOW)
                GPIO.output(green_led, GPIO.HIGH)
                print("GREEN LED ON!!")
           
                
        elif (chk is dht.DHTLIB_ERROR_CHECKSUM): #data check has errors
            print("Measurement inconclusive")
        elif (chk is dht.DHTLIB_ERROR_TIMEOUT):  #reading DHT times out
            print("Data timeout")
        else:               #other errors
            print("Other error!")          


        print("Humidity : %.2f, \t Temperature : %.2f \n"%(dht.humidity,dht.temperature))
        time.sleep(2)



    
if __name__ == '__main__':
    print ('Program is starting ... ')
    try:
        setup()
        loop()

    except KeyboardInterrupt:
        GPIO.cleanup()
        exit()
