# Using library for thingspeak from https://github.com/mchwalisz/thingspeak
import thingspeak
import time
import sys

# open and format write key

def get_key(filename, out=sys.stdout):
    out.write('using key path {}\n'.format(filename))
    with open(filename) as f:
        key = f.readline()
    out.write("- read key with {} chars\n".format(len(key.strip())))
    return key.strip()

humi = 22
temp = 30
channel_id = get_key('channel.txt') 
write_key = get_key('write_key.txt')


while True:
    #timesleep to match thingspeaks intervals 
    time.sleep(15)
    # establishing the connection to thingspeak
    channel = thingspeak.Channel(id=channel_id, api_key=write_key)
    # thingspeak {channel: value}
    response = channel.update({1: temp})
    print(response)
    time.sleep(15)
    response = channel.update({2: humi})
    print(response)

