# thingspeak example simple

Simple example of sending values to graphs in a thingspeak channel

# Development usage

You need Python above version 3.5, pip installed and a thingspeak.com account + channel 

1. Clone the repository `git clone git@gitlab.com:npes-py-experiments/thingspeak-example-simple.git` 
2. Create a virtual environment https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment 
2. Activate the virtual environment https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#activating-a-virtual-environment
3. Install requirements `pip install -r requirements.txt`
4. Copy your thingspeak channel id to a file named `channel.txt` , place it in the thingspeak-example folder
5. Copy your thingspeak write api key to a file named `channel.txt`, place it in the thingspeak-example folder   
4. Run `python3 thingspeak_example_simple.py` (linux) or `py thingspeak_example_simple.py` (windows)

Remember to set up enviorment!!!