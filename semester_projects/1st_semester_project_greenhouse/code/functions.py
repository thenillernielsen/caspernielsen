import sys

# open and format write key
def get_key(filename, out=sys.stdout):
    out.write('using key path {}\n'.format(filename))
    with open(filename) as f:
        key = f.readline()
    out.write("- read key with {} chars\n".format(len(key.strip())))
    return key.strip()