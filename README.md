Project portfolio by Casper Nielsen
This portfolio is the results of my work from the education IT Technology at UCL Erhvervsakademi

[[_TOC_]]

## :file_folder: Semester Projects
### 1. Semester Greenhouse Project
This was the first project on my education, these projects was made in group of students to emphasize the importance of working together. The main goal for this project was to develope a monitor for a greenhouse which could meassure temperature and humidity

Project overview: [greenhouse_folder] <br>
Original gitlab project: [greenhouse_project]<br>
Gitlab pages for project: [greenhouse_page]

### 2. Semester Datacenter project
This project was done in collaberation with the OME education from Fredericia maskimesterskole. The project was managed with the SCRUM model, where the "owners" were the OME's. The project itself was about monitoring the environment on a datacenter and the changes in temperature and airflow etc. 

Project overview: [datacenter_folder]
<br>
Original gitlab project: [datacenter_project]
<br>
Gitlab pages for project: [datacenter_page]

### 3. Semester Automatic Solar panel project
This project is in two parts. We have a solar panel controlled by actuators, the first assignement is to make the solar unable to be harmed from its own movements. The second assignment which my group is working on at this time, is to make a GUI and make the solar panel track to sun to get optimal energi output.         
<br>
Project overview: [solar_panel_folder]
<br>
Original gitlab project: [solar_panel_project]
<br>

## :file_folder: Industrial Application Projects
### Universal Robot Interfacing
This project is also something that my group is working on at this time. It is about interfacing with a univerdsal robot via the primary and/or secondary interface with TCP/IP.

Original gitlab project: [UR_project]


## :file_folder: Advanced Electronics Projects
### Adafruit BNO055 absolute orientation
Right now we are working on making a controller with the BNO055 which can tell , amongst other things position in 3D space and acceleration.                                                                  
<br>
Original gitlab project: [BNO055_project]
<br>

## :file_folder: Embedded Projects
### IoT Zigbee Development
Embedded was a class on both 1. and 2. semester, mostly it dealt with IoT and making all other subjects interact with eachother. This project was written in python, it allows the coordinator Zigbee module to communicate to the routers and endpoints which are also made of Zigbees.
<br>  
Project overview: [Zigbee_folder]
<br>


## :file_folder: Networking projects
I have in this subject handled virtual networks through the use of VMWare Workstation and physical networks with SRX240 running the Junos OS. I have covered topics like, DHCP, wiresharking, static routing and many more as can be viewed in the reports.
### Reports
Various reports and assignments carried out by me and other groupmembers
<br>
[networking_reports]

## :file_folder: PCB Production Projects
### RaspberryPi HAT
A project i'm currently working on the design a PCB to function as a HAT for a Raspberry Pi.

Original gitlab project: [PCB_project] 
<br>

## About me
:books: Studying IT-technology at UCL Odense in Denmark

:mag: Currently searching for an internship position to further develope my skills

:smile: Coming from a sales background i don't fit into the stereotype of an IT guy. So i hope to find a workplaceplace in the future that sees that as a plus. 

:bulb: I'm working on improving my programming and my skills in networking and eletronics.

:muscle: My current education is very broad. However i am looking forward to working with a future imployer to narrow my field and focus more on a specialized role.


:mailbox: For more information or a talk reach out on the links below

# :telephone_receiver: Contact Information
:computer: https://www.linkedin.com/in/casper-nielsen-7624141b6/
<br>
:email: canielsenit@gmail.com

### Links :link:
My Gitlab profile: [gitlab_profile]

## Tools
<code><img width="10%" src="https://www.vectorlogo.zone/logos/python/python-ar21.svg"></code>
<code><img width="10%" src="https://www.vectorlogo.zone/logos/raspberrypi/raspberrypi-ar21.svg"></code>
<code><img width="10%" src="https://www.vectorlogo.zone/logos/arduino/arduino-ar21.svg"></code>
<code><img width="10%" src="https://www.vectorlogo.zone/logos/gitlab/gitlab-ar21.svg"></code>
<code><img width="10%" src="https://www.vectorlogo.zone/logos/gnu_bash/gnu_bash-ar21.svg"></code>
<code><img width="10%" src="https://www.vectorlogo.zone/logos/json/json-ar21.svg"></code>
<br>
<code><img width="10%" src="https://www.vectorlogo.zone/logos/wireshark/wireshark-ar21.svg"></code>
<code><img width="10%" src="https://www.vectorlogo.zone/logos/qtio/qtio-ar21.svg"></code>
<code><img width="10%" src="https://www.vectorlogo.zone/logos/inkscape/inkscape-ar21.svg"></code>
<code><img width="10%" src="https://upload.wikimedia.org/wikipedia/commons/1/11/VMware_logo.svg"></code>
<code><img width="10%" src="https://indeni.com/wp-content/uploads/2018/03/Junos-Blog-Banner.png"></code>



[greenhouse_folder]: https://gitlab.com/thenillernielsen/caspernielsen/-/tree/main/semester_projects/1st_semester_project_greenhouse
[greenhouse_project]: https://gitlab.com/20a-itt1-project/team_b1
[greenhouse_page]: https://20a-itt1-project.gitlab.io/team_b1/

[datacenter_folder]: https://gitlab.com/thenillernielsen/caspernielsen/-/tree/main/semester_projects/2nd_semester_project_datacenter
[datacenter_project]: https://gitlab.com/21s-itt2-datacenter-students-group/team-b1
[datacenter_page]: https://21s-itt2-datacenter-students-group.gitlab.io/team-b1/ 

[solar_panel_folder]: https://gitlab.com/thenillernielsen/caspernielsen/-/tree/main/semester_projects/3rd_semester_project_solar_panel#notes
[solar_panel_project]: https://gitlab.com/21a-itt3-project-student-group/itt3_solar_panel_group_5

[UR_project]: https://gitlab.com/senker/ur_user_interface_project_group_8

[BNO055_project]: https://gitlab.com/SimonBjerre/itt3_electronics_project

[Zigbee_folder]: https://gitlab.com/thenillernielsen/caspernielsen/-/tree/main/embedded_systems

[networking_reports]: https://gitlab.com/thenillernielsen/caspernielsen/-/tree/main/Networking_projects

[PCB_project]: https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-casper_nielsen

[gitlab_profile]: https://gitlab.com/thenillernielsen